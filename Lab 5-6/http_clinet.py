import PySimpleGUI as sg
import urllib.request
import time

layout = [[sg.Text("Enter site name ")],
          [sg.Input(key='-INPUT-')],
          [sg.Button('Ok'), sg.Button('Quit')],
          [sg.Multiline(size=(999999, 1999), key='-OUTPUT-'), ]]
window = sg.Window('http getter', layout, size=(1000, 700))
while True:
    event, values = window.read()
    # See if user wants to quit or window was closed
    if event == sg.WINDOW_CLOSED or event == 'Quit':
        break
    # Output a message to the window
    try:
        print('LOAD!')
        doc = urllib.request.urlopen(values['-INPUT-'])
        httpsaqw = str(doc.read().decode())
    except:
        httpsaqw = "Неверно введен URL"
    window['-OUTPUT-'].update('Site : ' + httpsaqw )

# Finish up by removing from the screen
window.close()

#doc = urllib.request.urlopen("http://vk.com")
#print(doc.read().decode())